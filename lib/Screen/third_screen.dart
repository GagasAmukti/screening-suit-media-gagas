import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quickalert/quickalert.dart';
import 'package:suit_media_screening/Component/singleton.dart' as singleton;
import 'package:http/http.dart' as http;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'dart:convert';

class ThirdScreen extends StatefulWidget {
  ThirdScreen({Key? key}) : super(key: key);

  @override
  State<ThirdScreen> createState() => _ThirdScreenState();
}

class _ThirdScreenState extends State<ThirdScreen> {
  Map? mapResponse;
  List? listResponse;

  Future apicall() async {
    http.Response response;
    response = await http
        .get(Uri.parse("https://reqres.in/api/users?page=1&per_page=12"));
    if (response.statusCode == 200) {
      setState(() {
        mapResponse = json.decode(response.body);
        listResponse = mapResponse!['data'];
      });
    }
  }

  @override
  void initState() {
    apicall();
    super.initState();
  }

  Future<void> _refresh() {
    setState(() {
      apicall();
    });
    return Future.delayed(Duration(seconds: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        title: Text(
          "Third Screen",
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: _refresh,
          child: ListView.builder(
              itemCount: listResponse?.length,
              itemBuilder: (context, index) {
                if (listResponse != null) {
                  return Container(
                      margin: const EdgeInsets.only(bottom: 5),
                      child: Card(
                          margin:
                              EdgeInsets.symmetric(vertical: 3, horizontal: 15),
                          // margin: const EdgeInsets.fromLTRB(10, 8, 10, 8),
                          child: SizedBox(
                              height: MediaQuery.of(context).size.height * 0.12,
                              child: ListTile(
                                leading: ClipOval(
                                  child: Image.network(
                                    listResponse![index]['avatar'],
                                    width: MediaQuery.of(context).size.width *
                                        0.14,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                title: Container(
                                  margin: const EdgeInsets.only(top: 5),
                                  child: Text(
                                    listResponse![index]['first_name']
                                            .toString() +
                                        " " +
                                        listResponse![index]['last_name']
                                            .toString(),
                                    style: GoogleFonts.poppins(
                                      color: Color(0xFF04021D),
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20,
                                    ),
                                  ),
                                ),
                                subtitle: Text(
                                  listResponse![index]['email'].toString(),
                                  style: GoogleFonts.poppins(
                                    color: Color(0xFF686777),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15,
                                  ),
                                ),
                                selected: true,
                                selectedTileColor: const Color(0xFFffffff),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                onTap: () {
                                  QuickAlert.show(
                                    context: context,
                                    type: QuickAlertType.success,
                                    text: 'Berhasil Menambahkan UserName',
                                  );
                                  setState(() {
                                    singleton.userName = listResponse![index]
                                                ['first_name']
                                            .toString() +
                                        " " +
                                        listResponse![index]['last_name']
                                            .toString();
                                  });
                                },
                              ))));
                }
              }),
        ),
      ),
    );
  }
}
