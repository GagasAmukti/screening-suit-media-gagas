import 'package:flutter/material.dart';
import 'package:suit_media_screening/Component/button.dart';
import 'package:suit_media_screening/Component/text_field.dart';
import 'package:suit_media_screening/Screen/second_screen.dart';
import 'package:quickalert/quickalert.dart';
import 'package:scaffold_gradient_background/scaffold_gradient_background.dart';

class FirstScreen extends StatefulWidget {
  FirstScreen({Key? key}) : super(key: key);

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  final pallindromeController = TextEditingController();
  final namaController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ScaffoldGradientBackground(
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          Color(0xFF4ea4a9),
          Color(0xFF8e95ac),
          Color(0xFF2c618e),
        ],
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.25,
                height: MediaQuery.of(context).size.height * 0.2,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: const Color(0xff97c7cd)),
                child: Icon(
                  color: Color(0xfff2f2f2),
                  Icons.person_add_alt_1_sharp,
                  size: MediaQuery.of(context).size.width * 0.08,
                ),
              ),
              // logo

              SizedBox(height: MediaQuery.of(context).size.height * 0.05),

              // welcome back, you've been missed!

              // username textfield
              TextFieldInput(
                controller: namaController,
                obscureText: false,
                hintText: "Name",
              ),

              const SizedBox(height: 16),
              TextFieldInput(
                controller: pallindromeController,
                obscureText: false,
                hintText: "Pallindrome",
              ),

              // password textfield

              const SizedBox(height: 35),

              // sign in button
              ButtonNext(
                textButton: "CHECK",
                onTap: () {
                  showResult();
                },
              ),

              const SizedBox(height: 13),

              ButtonNext(
                textButton: "NEXT",
                onTap: () {
                  if (isNameFilled()) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              SecondScreen(nama: namaController.text)),
                    );
                  } else {
                    QuickAlert.show(
                        context: context,
                        type: QuickAlertType.error,
                        title: 'Oops...',
                        text: 'nama belum terisi');
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool isNameFilled() {
    if (namaController.text != "") {
      return true;
    }
    return false;
  }

  Future showResult() {
    if (isPalindrome(pallindromeController.text) &&
        pallindromeController.text != "") {
      return QuickAlert.show(
        context: context,
        type: QuickAlertType.success,
        text: 'Teks Merupakan Palindrome',
      );
    } else {
      return QuickAlert.show(
        context: context,
        type: QuickAlertType.error,
        title: 'Oops...',
        text: 'Teks Bukan Palindrome',
      );
    }
  }

  bool isPalindrome(String inputPallindrom) {
    return inputPallindrom == inputPallindrom.split("").reversed.join();
  }
}
