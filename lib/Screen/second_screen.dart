import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:suit_media_screening/Component/button.dart';
import 'package:suit_media_screening/Component/singleton.dart' as singleton;
import 'package:suit_media_screening/Screen/third_screen.dart';

class SecondScreen extends StatefulWidget {
  SecondScreen({Key? key, required this.nama}) : super(key: key);
  final String nama;
  final String userName = singleton.userName;
  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  @override
  void initState() {
    super.initState();
    init();
  }

  void init() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Second Screen",
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
      ),
      body: SafeArea(
          child: ListView(shrinkWrap: false, children: <Widget>[
        Container(
          //color: Colors.blueGrey,
          padding: const EdgeInsets.fromLTRB(15, 2, 0, 1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Welcome ',
                style: GoogleFonts.poppins(
                  color: Color(0xFF04021D),
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                ),
              ),
              Text(
                '${widget.nama}',
                style: GoogleFonts.poppins(
                  color: Color(0xFF04021D),
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.28),
        if (isUsernamePicked()) ...[
          Center(
            child: Text(
              singleton.userName,
              style: GoogleFonts.poppins(
                  color: Colors.black,
                  fontSize: 24,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ] else ...[
          Center(
            child: Text(
              "Selected User Name",
              style: GoogleFonts.poppins(
                  color: Colors.black,
                  fontSize: 24,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ],
        SizedBox(height: MediaQuery.of(context).size.height * 0.38),
        ButtonNext(
          textButton: "Choose a User",
          onTap: () {
            Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ThirdScreen()))
                .then((_) {
              // This block runs when you have come back to the 1st Page from 2nd.
              setState(() {
                // Call setState to refresh the page.
              });
            });
          },
        ),
      ])),
    );
  }

  bool isUsernamePicked() {
    if (singleton.userName != "") {
      return true;
    }
    return false;
  }
}
